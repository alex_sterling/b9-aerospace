# Installation

Unpack everything to the root folder of the game, overwriting already existing GameData and Ships folders.

Source folder contains the source code for the included plugins and is not necessary for the mod to work.

Make sure you have KSP 0.24.2 or higher.

Do *not*, under any circumstances, extract just `GameData/B9_Aerospace/Parts` folder alone, the mod won't work this way.

With a proper installation, you should have multiple new folders in GameData alongside with Squad folder that was there originally.

Do not try to move the part folders anywhere or sort them by category subfolders, that will break resource linking in all configs.

If you are experiencing out of memory crashes in the game with this mod, we recommend you install the Active Texture Management mod. You can find it here: [forums](http://forum.kerbalspaceprogram.com/threads/59005)

B9 Aerospace includes an up-to-date ATM config for B9, the one included in ATM should be removed.

B9 Aerospace requires Ferram Aerospace Research or NEAR. While the parts are balanced and will function in the stock model, it is unsupported, and the example craft provided will likely not even be able to take off.

See the `Aerodynamics` section for links.

___________________________________

# About

This set of parts vastly expands a selection of parts, in particular with spaceplane components such as new engines, cockpits, wings and fuselage systems.

The style was influenced by futuristic concept art, modern UAV designs, some believable hard sci-fi ideas, DARPA & NASA aerospace research, and by asymmetrical shapes and faceted designs that are nowadays common everywhere from automotive industry to military aircraft.

I have decided not to go overboard with realistic art style or extreme colors: my intention is to make parts which would blend seamlessly with, for example, KW Rocketry pieces.

My textures are hand-painted, I’m avoiding photo-sourced areas or excessive detail like rust.

Everything looks just a little bit cartoony, like a concept art, which, in my opinion, fits nicely with the general art style of KSP (most of the stock part textures were painted too).

Although I’m going for higher fidelity and texture resolution, so don’t expect a 100% match – but I’m fairly satisfied with results and have already built some neat ships using them.

The goal of the project is to fill every empty niche in KSP part selection and to replace every sub-par component with a new part better suited for the role.

This is the fourth milestone release of the project, now featuring 210 new parts and fully compatible with KSP 0.22.

Full changelog, support and development updates can be found in the [forum thread](http://forum.kerbalspaceprogram.com/threads/92630).



___________________________________

# Credits

## Core Team

* bac9 - 3D modeling, texturing, project lead
* Taverius - balance, 3D modeling, craft design, maintenance
* K3|Chris - 3D modeling, texturing, IVA


## Contributors and bundled mods

* Nazari - HotRockets Engine FX: [forums](http://forum.kerbalspaceprogram.com/threads/55219)
* Snjo - Firespitter.dll plugin v6.3.5: [forums](http://forum.kerbalspaceprogram.com/threads/24551)
* Kine - KineTechAnimation.dll plugin: [github](http://github.com/KineMorto/KineTechAnimationLibrary)
* Fel - ResGen.dll plugin v0.28.1: [forums](http://forum.kerbalspaceprogram.com/threads/28895)
* Sarbian - SmokeScreen.dll plugin v2.4.5.0: [forums](http://forum.kerbalspaceprogram.com/threads/71630)
* Ialdabaoth, Sarbian: ModuleManager v2.4.4: [forums](http://forum.kerbalspaceprogram.com/threads/55219)
* dtobi - KM_Gimbal.dll plugin v2.1.2: [github](http://github.com/dtobi/km_Gimbal)
* NathanKell - CrossFeedEnabler v3.0.2: [forums](http://forum.kerbalspaceprogram.com/threads/76499)
* Mihara - RasterPropMonitor v0.18.2: [forums](http://forum.kerbalspaceprogram.com/threads/57603)
* Greys - VirginGeneric.dll v1.6.1: [github](http://github.com/Greys0/Virgin-Kalactic)
* jadebenn, Hyomoto, Helldiver - RPM MFD configuration.
* alexustas - RPM MFD model, configuration.
* FPSlacker - HotRockets exhaust flame atmospheric compensation.

## Special thanks
For answering tons of silly questions.

* DYJ
* C7
* r4m0n
* Sarbian
* NathanKell
* egg
* snjo
* ferram4
* Mu



___________________________________

# License

This work is shared under CC BY-NC-SA 3.0 license. [link](https://creativecommons.org/licenses/by-nc-sa/3.0/)

Components and included code & content may come under their own license. Please check their documentation or their sites.



___________________________________

# Aerodynamics

While we take pains to ensure the parts are balanced in the stock drag model, we all use either NEAR or FAR. The stock drag model is unsupported.

For the best possible experience with spaceplanes in Kerbal Space Program, you will need to install one of these two mods:



## Ferram Aerospace Research (FAR)

The true accurate and realistic aerodynamics for KSP.

Can be somewhat daunting for beginners. [forum](http://forum.kerbalspaceprogram.com/threads/20451)


## Neophyte's Elementary Aerodynamics Replacement (NEAR)

FAR with the complicated bits removed. [forum](http://forum.kerbalspaceprogram.com/threads/86419)

Features from FAR missing in NEAR:

* Changes in physics with Mach number
* Complicated changes in wing lift and drag due to other parts around them
* Aerodynamic dis-assembly (though they can still be broken off if they overload the stock joints)
* Complicated aerodynamic analysis tools in the editor

Please note that most of the features missing in NEAR can be disabled in FAR.


___________________________________

# Suggestions

These are some of the mods we like to use when we play KSP, in alphabetical order:



## Aligned Currency

Should have been in stock. Simple mod that makes the amount of kerbucks you have line up properly ... [forum](http://forum.kerbalspaceprogram.com/threads/88027)



## Alternate Resource Panel

One that's actually useful and informative. [forum](http://forum.kerbalspaceprogram.com/threads/60227)



## Active Texture Management

Automatically compress textures and reduce them in size. No need for reduced texture packs any longer.

Highly configurable. [forum](http://forum.kerbalspaceprogram.com/threads/59005)



## Chatterer

Cute little plugin that plays garbled sounds and radio beeps like you're in a *real* space program. [forum](http://forum.kerbalspaceprogram.com/threads/25367)



## Coherent Contracts

Squad are from Mexico so we won't hold it against them, but the contracts text sometimes doesn't make grammatical sense. This fixes that problem. [forum](http://forum.kerbalspaceprogram.com/threads/88149)



## Contracts Window +

Simply put, a (much) better contracts window. [forum](http://forum.kerbalspaceprogram.com/threads/91034)



## CrossFeed Enabler

Lets radially-mounted fuel tanks feed *into* their root part. The full mod is included in B9, because its a little pearl of genius. [forum](http://forum.kerbalspaceprogram.com/threads/76499)



## Custom Biomes

More places to do science at! [forum](http://forum.kerbalspaceprogram.com/threads/66256)



## DebRefund

Get money back for those boosters that you manage to parachute safely back to kerbin. Another one of those 'should have been in stock' features. [forum](http://forum.kerbalspaceprogram.com/threads/86691)



## DMagic Orbital Science

More instruments to do science *with*. Also new science-gathering contracts. Compatible with (and recommends) Custom Biomes. [forums](http://forum.kerbalspaceprogram.com/threads/64972)



## Docking Port Alignment Indicator

A proper interface for accurately docking. [forums](http://forum.kerbalspaceprogram.com/threads/43901)



## Editor Extensions

Much needed extra functionality for the VAB/SPH.

After you've used it you won't be able to do without. [forums](http://forum.kerbalspaceprogram.com/threads/38768)



## Editor Part Highlighter

Make the editor highlight of parts from the staging list more obvious. [forums](http://forum.kerbalspaceprogram.com/threads/50143)



## Enhanced Navball

Extra markers on the navball for Radial, Normal and anti-maneuver. Ghosting for Pro/Retrograde and Maneuver nodes.

Navball functionality that Squad wants to include but hasn't got round to doing yet. [forum](http://forum.kerbalspaceprogram.com/threads/50524)



## Extraplanetary Launchpads

Mine for metal on planets and moons and build rockets with them.

Use together with Kethane or (with patch) Karbonite to create your Kerbal Space Empire. [forum](http://forum.kerbalspaceprogram.com/threads/59545)



## EVA Parachutes

Chutes for your kerbals so they can bail from damaged spaceplanes and such. [forums](http://forum.kerbalspaceprogram.com/threads/25305)



## Fine Print

More types of contracts for you to fail at! A must-have for a campaign game. [forums](http://forum.kerbalspaceprogram.com/threads/88445)



## Firespitter

WW2-era aircraft and helicopter parts, seaplane floats.

The FS plugin enables lots of advanced functionality in B9 (and other mods) and he's done a lot of work just because we asked, so go check it out, and give snjo some encouragement. [forum](http://forum.kerbalspaceprogram.com/threads/24551)



## Floor It

You know how 'x' kills the throttle? Well this little gem adds 'z' for full throttle. [curseforge](http://kerbal.curseforge.com/ksp-mods/221031)


## Gimbal Auto Trim

Extends the default Gimbal Module.

When activated, engines will orient so that the Direction of Thrust is aligned with the Centre of Mass.[forums](http://forum.kerbalspaceprogram.com/threads/77710)


## HotRockets!

We use its particle FX to make our engines look nice.

Get the main mod to make other engines look awesome too.

Supports lots of major mods as well as the Squad engines. Both HR and B9 use Sarbian's SmokeScreen, use whichever version of the plugin is the most recent.

HotRockets effects are included in B9, so make sure to remove any B9-specific HotRockets configs.
[forum](http://forum.kerbalspaceprogram.com/threads/65754)



## Improved Chase Camera

An improved chase camera for planes.

Can be toggled at any time to return to the default functionality. [forum](http://forum.kerbalspaceprogram.com/threads/80112)



## Infernal Robotics

The continuation of the venerable Damned Robotics.

Servo motors, hydraulic rams and other mechanical wizardry for building folding planes, bipedal robots, cargo gantries, and anything else you can think of that needs to rotate or fold or extend or contract. [forums](http://forum.kerbalspaceprogram.com/threads/37707)



## Karbonite

A resource system alternative to Kethane, using the Open Resource System from KSP Interstellar. [forums](http://forum.kerbalspaceprogram.com/threads/89401)

Compatibility patch for Extraplanetary Launchpads: [forums](http://forum.kerbalspaceprogram.com/threads/89774)



## Kethane

The Grand-daddy of mineable resource systems. [forum](http://forum.kerbalspaceprogram.com/threads/23979)



## Kerbal Aircraft Expansion

Modern propeller/turboprop/helicopter engines and landing gear, powered by FireSpitter. [forums](http://forum.kerbalspaceprogram.com/threads/76668) 



## Kerbal Joint Reinforcement

Improvement to the underlying physics of KSP.

While many parts of this have been made redundant by improvements in KSP itself (made as a result of this mod) the parts that remain are still extremely useful. [forums](http://forum.kerbalspaceprogram.com/threads/55657)



## Kerbal Alarm Clock

Create in-game reminders so you don't miss important events, like SOI changes, periapsis/apoapsis, custom orbit points, and much more.

A fundamental mod. [forums](http://forum.kerbalspaceprogram.com/threads/24786)



## Kerbal Attachment System

Winches, magnets, tubes, EVA functionality.

Do a Curiosity-like flying crane rover landing, airlift splashed-down capsules with your helicopter, connect refueling pipes in EVA.

Use it with Kethane/Karbonite and EPL to connect your remote, self-built KSC together. [forums](http://forum.kerbalspaceprogram.com/threads/53134)

Patch for 0.24.2: [forums](http://forum.kerbalspaceprogram.com/threads/53134!?p=1319122&viewfull=1#post1319122)



## Kerbal Engineer Redux

If you're not a fan of mechjeb-style automation, but you still want all that information that stock KSP should give you but does not, this is what you want - more information than you can shake a rocket at. [forums](http://forum.kerbalspaceprogram.com/threads/18230)



## KSP Interstellar

Advanced propulsion systems, generators and resource gathering.

Nuclear reactors, plasma thrusters, all the way up to antimatter harvesting and warp drives.

Uses the same Open Resource System as Karbonite. [github](http://github.com/WaveFunctionP/KSPInterstellar)



## KSP Stock Tweaks

4 plugins to set some defaults:

* Set free cam as default
* Set precision control as default
* Map shows navball by default
* Precision mode applies to joysticks

All can be used independently of each other. [forums](http://forum.kerbalspaceprogram.com/threads/80951)



## KW Rocketry

Our favourite rocket parts expansion mod - rocket parts done *right*. 'nuff said. [forums](http://forum.kerbalspaceprogram.com/threads/51037)



## Lack Luster Labs Pack

Large spacecraft/station parts, with habitations modules.

Goes well with the HX parts, so much we made an adapter from one to the other. [forums](http://forum.kerbalspaceprogram.com/threads/79542)



## MechJeb2

Some purists like to fly everything manually like real astronauts never did.

If you're not one of these people, or like us you've flown so many moon landings manually its just plain gotten old, this is the grand-daddy of autopilots, with more features than we could possibly list here. [forums](http://forum.kerbalspaceprogram.com/threads/12384)

FAR/NEAR/KM_Gimbal compatibility extensions: [forums](http://forum.kerbalspaceprogram.com/threads/60933)



## NavHud

A navball-inspired Head Up Display for when you're not IVA (or maybe you just don't like flying IVA). [forums](http://forum.kerbalspaceprogram.com/threads/81543)



## NavUtilities

Better instruments for flight. RPM-compatible. [forums](http://forum.kerbalspaceprogram.com/threads/85353)



## Part Angle display

See and set the rotations of parts in the editor, and change the fine angle changes from the stock value down all the way down to 0.01 degrees at a time if you need to.

Widely used to create the stock B9 crafts, because 5 degrees is a bit too coarse for planes. [forums](http://forum.kerbalspaceprogram.com/threads/81390)



## Precise Node

For when the adjustments you're trying to do to a maneuver node are just too small for the default interface. [forums](http://forum.kerbalspaceprogram.com/threads/47863)



## Procedural Fairings

Payload fairings that automatically resize.

You can reshape and tweak them in the VAB, and use it to make custom inter-stage fairings as well. [forum](http://forum.kerbalspaceprogram.com/threads/39512)



## Procedural Wings

Wings and control surfaces you can scale and reshape in the VAB/SPH.

Give your plane the wings you imaged, or make the winglets on your rocket just right. [forum](http://forum.kerbalspaceprogram.com/threads/29862)



## Protractor Continued

A classic mod that does one thing and does it right - calculates the optimum departure angles for your interplanetary transfers. [forums](http://forum.kerbalspaceprogram.com/threads/83173)



## Raster Prop Monitor

Impressive Multi-function displays for IVA cockpit views.

Makes IVA-only piloting possible. Supports many major mods as well as Squad and B9 cockpits. [forum](http://forum.kerbalspaceprogram.com/threads/57603)



## RCS Build Aid

Not just for RCS! Extra markers in the VAB/SPH.

Centre of Mass for empty fuel as well as full. See torque effects of off-centre engine and RCS thrust when designing crafts.

A great help for avoiding nasty surprises with changing center of mass and off-balance RCS. [forum](http://forum.kerbalspaceprogram.com/threads/35996)



## RealChute Parachute Systems

Improved parachute mechanics.

Gradual deployment, more deployment conditions, editable in vab, and more. [forums](http://forum.kerbalspaceprogram.com/threads/57988)



## SCANsat

Satellite scanner parts to create detailed in-game maps of planets and moons.

Altitude, slope, biome and anomaly maps with zoom to help you plan your landings or look for that missing monolith. Supports or is supported by Kethane, Karbonite, KSPi, RPM so you can map all your resources with one system and see it in the cockpit. [forum](http://forum.kerbalspaceprogram.com/threads/80369)



## Science Alert

Lets you know when you've crossed biome borders so you know to run all those experiments. [forums](http://forum.kerbalspaceprogram.com/threads/76793)



## Space Shuttle Engines

Engines for your SLS/Buran-alikes.

We use the Gimbal plugin from this mod, and its *awesome*. It really does let you build stuff with off-center thrust like the Shuttle Launch System.

Also has some very nice replica main tanks, and utility parts that can trigger staging/action groups based on conditions, there's timers, fuel crossfeed controllers and more. [forum](http://forum.kerbalspaceprogram.com/threads/55985)



## TAC Fuel Balancer

Automatically balance fuel between tanks, dump fuel, transfer a resource from/to all other tanks, or lock it so resources can't move in or out of it. Highly configurable.

Recommended for any SSTO or plane pilot, it will make keeping your planes balanced as engines draw fuel much easier. [forum](http://forum.kerbalspaceprogram.com/threads/25823)



## TAC Life Support

Resource needs for your kerbals, for those that want a more realistic space program.

Compatible with USI Kolonization System [forums](http://forum.kerbalspaceprogram.com/threads/40667)



## Toolbar

The original plugin toolbar, and still superior to the one added to stock in 0.24.

While most plugins that can use it include it, its nice to know where to check for the latest version. [forums](http://forum.kerbalspaceprogram.com/threads/60863)



## USI Kolonization System

Parts and plugins targeted specifically at setting up remote bases for your kerbals to live and work in. Uses the Open Resource System like KSPi and Karbonite. Compatible (and recommends) TAC Life Support. [forums](http://forum.kerbalspaceprogram.com/threads/79588)



## Vessel Viewer

Awesome plugin that lets you examine and interact with your craft's parts while IVA. Compatible with RPM. [forums](http://forum.kerbalspaceprogram.com/threads/80581)